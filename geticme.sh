#!/usr/bin/env bash

# This script is not portable as it depends on a specific ssh config
# value and makes use of the 'open' command for use on Mac OS X.

# Tested on Mac OS 10.7.4

# Description: initiate the analyses remotely then package, fetch, and
# display the resulting figures.


ICME=icme
REMOTE_ROOT='cabdulwa/hipin'

run_icme() {
	directory="$1"
	cmd="$2"
	ssh $ICME "cd $directory && $cmd"
}

get_icme() {
	path=$1
	name=$(basename $path)
	scp $ICME:$path $name
	tar xvf $name
	rm $name
}

run_and_get() {
	### 'name' is the analysis name, eg 'rmsds' or 'sstruct'
	### This assumes that there is a 'calc-$name.sh' in $REMOTE_ROOT that produces '$name.tar.bz2'

	### 'patterns' is a (potentially wildcarded) group of files to call 'open' on

	### ex: get_rmsds() { run_and_get rmsds 'rmsds/*-folded.png' 'rmsds/*-ext.png' }
	### This will execute "calc-rmsds.sh' in $REMOTE_ROOT on $ICME, scp and expand the 'rmsds.tar.bz2' archive, then open the folded and extend png files groups.

	name=$1
	shift
	patterns="$@"

	rm -rf $name
	run_icme "$REMOTE_ROOT" "./calc-$name.sh"
	get_icme "$REMOTE_ROOT/$name.tar.bz2"

	### -f/+f ('f' is the 'noglob' bash option)
	### This disables/enables globbing.

	set -f # prevent $patterns from expanding
	for p in $patterns; do
		set +f # turn globbing back on otherwise 'open' cannot find "foo/*-bar.baz"
		open $p
		set -f
	done
	set +f

}

get_rmsds() {
	run_and_get \
		rmsds \
		'rmsds/*-folded.png' \
		'rmsds/*-ext.png' \
		'rmsds/*-folded-rfrac.png' \
		'rmsds/*-ext-rfrac.png'

}

get_sstruct() {
	run_and_get \
		sstruct \
		'sstruct/*-folded/workarea-*/ss.png' \
		'sstruct/*-ext/workarea-*/ss.png'
}

get_all() {
	get_rmsds
	get_sstruct
}

usage() {
	echo "Usage: $1 [rmsds|sstruct|all]"
	exit 1
}


case $1 in
	rmsds)
		get_rmsds
		;;
	sstruct)
		get_sstruct
		;;
	all)
		get_all
		;;
	*)
		echo "Unknown switch '$1'"
		usage
		;;
esac
